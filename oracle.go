package oracle

import (
	"errors"
	"fmt"
	_ "github.com/godror/godror"
	"gitlab.com/megafon-libraries/database"
	"gitlab.com/megafon-libraries/kv"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Oracle struct {
	extractor  func(string) (kv.Cache, error)
	zoneOffset int
	database.DB
}

func (db *Oracle) Init() *database.DB {
	_, db.zoneOffset = time.Now().Zone()
	db.SetDatabaseInfo("type", "oracle")
	db.DB.SetDriver("godror")
	db.DB.SetParent(db)
	return &db.DB
}

func (db *Oracle) SetConnectExtractor(extractor func(string) (kv.Cache, error)) {
	db.extractor = extractor
}

func (db *Oracle) ConnectExtract(connect string) (string, error) {
	if db.extractor == nil {
		return connect, errors.New("no extraction function set")
	}
	data, err := db.extractor(connect)
	db.SetDatabaseInfo("tns", data["tns"])
	db.SetDatabaseInfo("database", data["tns"])
	db.SetDatabaseInfo("schema", data["username"])
	db.SetDatabaseInfo("username", data["username"])
	return fmt.Sprintf("%s/%s@%s", data["username"], data["password"], data["tns"]), err
}

func (db *Oracle) Connect(connect string) error {
	db.Init()
	return db.DB.Connect(connect)
}

func (db *Oracle) ErrorFilter(err error) error {
	if err != nil {
		err = errors.New(string(regexp.MustCompile("(params={{.*?}}\\s*)").ReplaceAll([]byte(err.Error()), []byte(""))))
	}
	return err
}

func (db *Oracle) ColumnTypeTransform(col interface{}, columnType string) interface{} {
	switch strings.ToUpper(columnType) {
	case "NUMBER":
		if col != nil {
			value, ok := col.(string)
			if ok {
				data, err := strconv.ParseInt(value, 10, 64)
				if err == nil {
					col = data
				}
				if strconv.FormatInt(data, 10) != value {
					if data, err := strconv.ParseFloat(value, 64); err == nil {
						col = data
					}
				}
			}
		}
	case "VARCHAR2":
		col = col.(string)
	case "DATE":
		if col != nil {
			value, ok := col.(time.Time)
			if ok {
				col = value.UTC().Add(time.Second * time.Duration(int64(db.zoneOffset)))
			}
		}
	}
	return col
}
