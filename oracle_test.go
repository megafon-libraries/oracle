package oracle

import (
	"bou.ke/monkey"
	"errors"
	"github.com/stretchr/testify/require"
	"gitlab.com/megafon-libraries/database"
	"gitlab.com/megafon-libraries/kv"
	"reflect"
	"testing"
	"time"
)

func init() {

	//	Patch Connect function
	var guard *monkey.PatchGuard
	var dbSQL *database.DB
	guard = monkey.PatchInstanceMethod(reflect.TypeOf(dbSQL), "Connect", func(db *database.DB, str string) error {
		guard.Unpatch()
		defer guard.Restore()
		return errors.New("dummy")
	})
}

func TestOracle_Init(t *testing.T) {
	db := Oracle{}
	db.Init()
	db1 := (*db.DB.Parent()).(*Oracle)
	require.Equal(t, &db, db1, "Init failed")
	require.Equal(t, db.DB.DatabaseInfo("type"), "oracle")
	require.Equal(t, db.DatabaseInfo("type"), "oracle")
}

func TestOracle_ConnectExtractor(t *testing.T) {
	db := Oracle{}
	db.Init()
	extract := func(string) (kv.Cache, error) {
		return map[string]string{"username": "user", "password": "pass", "tns": "tns"}, nil
	}
	_, err := db.ConnectExtract("test")
	require.Error(t, err, "Connect extract failed")
	require.EqualError(t, err, "no extraction function set", "Connect extract failed")
	db.SetConnectExtractor(extract)
	result, _ := db.ConnectExtract("test")
	require.Equal(t, "user/pass@tns", result, "Connect extract failed")
	extract = func(string) (kv.Cache, error) {
		return map[string]string{}, errors.New("passed")
	}
	db.SetConnectExtractor(extract)
	_, err = db.ConnectExtract("test")
	require.Error(t, err, "Connect extract failed")
	require.EqualError(t, err, "passed", "Connect extract failed")
}

func TestOracle_Connect(t *testing.T) {
	db := Oracle{}
	db.Init()
	err := db.Connect("error")
	require.Error(t, err, "Connect extract failed")
	require.EqualError(t, err, "dummy", "Invalid connect")
}

func TestOracle_ErrorFilter(t *testing.T) {
	db := Oracle{}
	err := db.ErrorFilter(nil)
	require.NoError(t, err, "Error filter failed")
	err = db.ErrorFilter(errors.New("one two three params={{four}} five"))
	require.Error(t, err, "Error filter failed")
	require.EqualError(t, err, "one two three five", "Invalid filter failed")
}

func TestOracle_ColumnTypeTransform(t *testing.T) {
	var result interface{}
	db := Oracle{}
	result = db.ColumnTypeTransform("12.5", "NUMBER")
	require.Equal(t, 12.5, result, "Invalid type transformation")
	result = db.ColumnTypeTransform("12", "NUMBER")
	require.Equal(t, int64(12), result, "Invalid type transformation")
	result = db.ColumnTypeTransform("test", "VARCHAR2")
	require.Equal(t, "test", result, "Invalid type transformation")
	tme := time.Now().UTC()
	result = db.ColumnTypeTransform(tme, "DATE")
	require.Equal(t, tme, result, "Invalid type transformation")
}